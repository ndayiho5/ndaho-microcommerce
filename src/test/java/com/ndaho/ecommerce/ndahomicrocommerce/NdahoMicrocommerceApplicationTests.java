package com.ndaho.ecommerce.ndahomicrocommerce;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Disabled;

@SpringBootTest
@Disabled("Class not ready for tests")
class NdahoMicrocommerceApplicationTests {

	@Test
	void contextLoads() {
	}

}
