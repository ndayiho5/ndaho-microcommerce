package com.ndaho.ecommerce.ndahomicrocommerce.integration.controller;

import com.ndaho.ecommerce.ndahomicrocommerce.web.exceptions.ProduitGratuitException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Objects;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class ProductMicroserviceControllerTestIT {

    @Autowired
    private MockMvc mvc;

    @Test
    public void givenProducts_whenGetProducts_thenStatus200() throws Exception {
        mvc.perform(get("/Produits").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].nom", is("Ordinateur portable")));
    }

    @Test
    public void givenProducts_whenGetProducts_thenProductsASCSortedByName() throws Exception {
        mvc.perform(get("/TriProduits").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].nom", is("Aspirateur Robot")))
                .andExpect(jsonPath("$[1].nom", is("Ordinateur portable")))
                .andExpect(jsonPath("$[2].nom", is("Table de Ping Pong")));
    }

    @Test
    public void givenProducts_whenGetProducts_thenProductsDESCSortedByName() throws Exception {
        mvc.perform(get("/TriProduits").contentType(MediaType.APPLICATION_JSON)
                        .param("sortBy", "DESC"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].nom", is("Table de Ping Pong")))
                .andExpect(jsonPath("$[1].nom", is("Ordinateur portable")))
                .andExpect(jsonPath("$[2].nom", is("Aspirateur Robot")));
    }


    @Test
    public void givenProducts_whenGetProductsMarge_thenProductsMargeAreCalculated() throws Exception {
        mvc.perform(get("/AdminProduits").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{\n" + "    \"Product(id=2, nom=Aspirateur Robot, prix=500, prixAchat=200)\": 300,\n" + "    \"Product(id=3, nom=Table de Ping Pong, prix=750, prixAchat=400)\": 350,\n" + "    \"Product(id=1, nom=Ordinateur portable, prix=350, prixAchat=120)\": 230\n" + "}"));

    }

    @Test
    public void givenProducts_whenGetFormatedProductsMarge_thenProductsMargeAreCalculated() throws Exception {
        mvc.perform(get("/AdminFormatedProduits").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(3)))
                //  TODO :use json file instead
                .andExpect(jsonPath("$[0].product.nom", is("Ordinateur portable")))
                .andExpect(jsonPath("$[0].marge", is(230)));
    }

    @Test
    public void givenProduct_whenAddNewProductWithZeroPrice_thenThrowProduitGratuitException() throws Exception {
        mvc.perform(post("/Produits")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"nom\": \"Ecoute\",\n" +
                                "    \"prix\": 0,\n" +
                                "    \"prixAchat\": 500\n" +
                                "}"))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ProduitGratuitException))
                .andExpect(result -> assertEquals("Le prix de vente ne peut pas etre zero", Objects.requireNonNull(result.getResolvedException()).getMessage()));
    }

}