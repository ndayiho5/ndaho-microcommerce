
DROP TABLE IF EXISTS product;

CREATE TABLE product
(
    id         INT PRIMARY KEY  AUTO_INCREMENT,
    nom        VARCHAR(255) NOT NULL,
    prix       INT NOT NULL,
    prix_achat INT          NOT NULL
);