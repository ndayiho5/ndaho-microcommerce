package com.ndaho.ecommerce.ndahomicrocommerce.web.controller;

import com.ndaho.ecommerce.ndahomicrocommerce.model.MargeProductResponse;
import com.ndaho.ecommerce.ndahomicrocommerce.model.Product;
import com.ndaho.ecommerce.ndahomicrocommerce.web.exceptions.ProduitGratuitException;
import com.ndaho.ecommerce.ndahomicrocommerce.web.exceptions.ProduitIntrouvableException;
import com.ndaho.ecommerce.ndahomicrocommerce.web.service.ProductMicroserviceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Api("API pour les opérations CRUD sur les produits.")
@RestController
@Slf4j
public class ProductMicroserviceController {
    @Autowired
    ProductMicroserviceService productMicroserviceService;

    @GetMapping("/Produits")
    public List<Product> listeProduits() {
        return productMicroserviceService.findAll();
    }

    @ApiOperation(value = "Récupère un produit grâce à son ID à condition que celui-ci soit en stock!")
    @GetMapping("/Produits/{id}")
    public Product afficherUnProduit(@PathVariable int id) {
        Product product = productMicroserviceService.findById(id);
        if (product == null) {
            throw new ProduitIntrouvableException("Le produit avec l'id " + id + " est INTROUVABLE. Écran Bleu si je pouvais.");
        }
        return product;
    }

    @PostMapping(value = "/Produits")
    public ResponseEntity<Product> ajouterProduit(@RequestBody Product product) throws ProduitGratuitException {
        if (product.getPrix() == 0) {
            log.error("Le prix de vente ne peut pas etre zero");
            throw new ProduitGratuitException("Le prix de vente ne peut pas etre zero");
        }
        Product productAdded = productMicroserviceService.create(product);
        if (Objects.isNull(productAdded)) {
            return ResponseEntity.noContent().build();
        }
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(productAdded.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping(value = "/Produits/{id}")
    public void supprimerProduit(@PathVariable int id) {
        productMicroserviceService.deleteById(id);
    }

    @PutMapping(value = "/Produits")
    public void updateProduit(@Valid @RequestBody Product product) {
        productMicroserviceService.update(product);
    }

    @GetMapping(value = "test/produits/{prixLimit}")
    public List<Product> testeDeRequetes(@PathVariable int prixLimit) {
        return productMicroserviceService.findByPrixGreaterThan(prixLimit);
    }

    @GetMapping(value = "TriProduits")
    public List<Product> trierProduitsParOrdreAlphabetique(@RequestParam(name = "sortBy", required = false) String sortMode) {
        return productMicroserviceService.getSortedProducts(sortMode);
    }

    @GetMapping(value = "AdminProduits")
    public Map<Product,Integer> calculerMargeProduit() {
        return productMicroserviceService.getProductsMargeResponse();
    }

    @GetMapping(value = "AdminFormatedProduits")
    public List<MargeProductResponse> calculerMargeProduitResponse() {
        return productMicroserviceService.getProductsFormatedMargeResponse();
    }
}
