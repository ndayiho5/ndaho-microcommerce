package com.ndaho.ecommerce.ndahomicrocommerce.web.repository;

import com.ndaho.ecommerce.ndahomicrocommerce.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    Product findById(int id);

    List<Product> findByPrixGreaterThan(int prixLimit);

    @Query(value = "SELECT * FROM Product p WHERE p.prix > :prixLimit", nativeQuery = true)
    List<Product> chercherUnProduitCher(@Param("prixLimit") int prix);

    List<Product> findByOrderByNomAsc();

    List<Product> findByOrderByNomDesc();
}
