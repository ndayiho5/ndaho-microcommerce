package com.ndaho.ecommerce.ndahomicrocommerce.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Le produit n'est pas etre gratuit")
public class ProduitGratuitException extends Exception {
    public ProduitGratuitException(String message) {
        super(message);
    }
}
