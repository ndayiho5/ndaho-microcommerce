package com.ndaho.ecommerce.ndahomicrocommerce.web.service;


import com.ndaho.ecommerce.ndahomicrocommerce.model.MargeProductResponse;
import com.ndaho.ecommerce.ndahomicrocommerce.model.Product;
import com.ndaho.ecommerce.ndahomicrocommerce.web.repository.ProductRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductMicroserviceService {

    @Autowired
    ProductRepository productRepository;

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public Product findById(int id) {
        return productRepository.findById(id);
    }

    public Product create(Product product) {
        return productRepository.save(product);
    }

    public void deleteById(int id) {
        productRepository.deleteById(id);
    }

    public void update(Product product) {
        productRepository.save(product);
    }

    public List<Product> findByPrixGreaterThan(int prixLimit) {
        return productRepository.chercherUnProduitCher(prixLimit);
    }

    public List<Product> getSortedProducts(String sortMode) {
        if (StringUtils.isNotBlank(sortMode) && sortMode.equalsIgnoreCase("DESC")) {
            return productRepository.findByOrderByNomDesc();
        } else {
            return productRepository.findByOrderByNomAsc();
        }
    }

    public Map<Product, Integer> getProductsMargeResponse() {
        List<Product> products = productRepository.findAll();
        Map<Product, Integer> margeProductResponseMap = new HashMap();
        for (Product product : products) {
            final Integer marge = product.getPrix() - product.getPrixAchat();
            margeProductResponseMap.put(product, marge);

        }
        return margeProductResponseMap;
    }

    public List<MargeProductResponse> getProductsFormatedMargeResponse() {
        List<Product> products = productRepository.findAll();
        List<MargeProductResponse> margeProductResponseList = new ArrayList<>();

        for (Product product : products) {
            MargeProductResponse margeProductResponse = new MargeProductResponse();
            final int marge = product.getPrix() - product.getPrixAchat();
            margeProductResponse.setProduct(product);
            margeProductResponse.setMarge(marge);
            margeProductResponseList.add(margeProductResponse);
        }
        return margeProductResponseList;
    }
}
