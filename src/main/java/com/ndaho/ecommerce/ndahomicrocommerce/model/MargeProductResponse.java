package com.ndaho.ecommerce.ndahomicrocommerce.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MargeProductResponse {

    private Product product;
    private int marge;

}
