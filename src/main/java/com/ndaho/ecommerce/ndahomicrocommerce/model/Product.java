package com.ndaho.ecommerce.ndahomicrocommerce.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Getter
@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
//@JsonIgnoreProperties(value = {"prixAchat", "id"}) -->pratique quand vous avez beaucoup de choses à cacher...
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Size(min = 3, max = 25)
    private String nom;
    @Min(value = 1)
    private int prix;
    @JsonIgnore //cacher cette property dans la réponse
    @Column(name="prix_achat")
    private int prixAchat;

}
