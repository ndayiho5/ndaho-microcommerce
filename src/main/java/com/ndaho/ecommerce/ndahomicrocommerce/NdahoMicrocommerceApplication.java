package com.ndaho.ecommerce.ndahomicrocommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class NdahoMicrocommerceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NdahoMicrocommerceApplication.class, args);
	}

}
