FROM eclipse-temurin:11-jdk-alpine

COPY docker/bootstrap.properties  target/ndaho-microcommerce.jar /opt/

WORKDIR /opt/

CMD java ${JAVA_OPTS} -jar ndaho-microcommerce.jar 
